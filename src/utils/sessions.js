/* eslint-disable no-undef */
export function setUserLang(lang) {
  return sessionStorage.setItem('heyWatchMe', JSON.stringify(lang))
}
export function getUserlang() {
  const lang = JSON.parse(sessionStorage.getItem('heyWatchMe'))
  if (lang === null) {
    setUserLang('en')
    return 'en'
  } else {
    return lang
  }
}
