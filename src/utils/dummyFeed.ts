export const dummyFeed = [
  {
    id: 'NEWSFEED-1',
    name: 'Jacklean',
    message: 'Tomorrow I will reveal the Goddess Of love and sensuality!',
    avatar: 'https://randomuser.me/api/portraits/women/59.jpg',
    time: '20 min ago',
    image: 'https://i.picsum.photos/id/212/300/300.jpg',
  },
  {
    id: 'NEWSFEED-2',
    name: 'Harry Potter',
    message: 'Lockdown makes me dance!!',
    avatar: 'https://randomuser.me/api/portraits/women/48.jpg',
    time: '1 hour ago',
    image: 'https://i.picsum.photos/id/440/300/300.jpg',
  },
  {
    id: 'NEWSFEED-3',
    name: 'Emma Jack',
    message:
      'Watch me dance for you. I will breathe heavy with you..and make you come with me..deeper and deeper..',
    avatar: 'https://randomuser.me/api/portraits/women/76.jpg',
    time: '1 hour ago',
    image: 'https://i.picsum.photos/id/22/300/300.jpg',
  },
]
