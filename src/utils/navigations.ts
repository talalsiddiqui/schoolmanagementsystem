import {
  HomeOutlined,
  EditOutlined,
  CalendarOutlined,
  MoneyCollectOutlined,
  UsergroupAddOutlined,
  UserOutlined,
  UnlockOutlined,
  FileWordOutlined,
} from '@ant-design/icons'

export const adminNavigation = [
  {
    id: 'home',
    title: 'Home',
    icon: HomeOutlined,
  },
  {
    id: 'accounts',
    title: 'Authorize',
    icon: UnlockOutlined,
  },
  {
    id: 'studentFees',
    title: 'studentFees',
    icon: MoneyCollectOutlined,
  },
  {
    id: 'parameters',
    title: 'Parameters',
    icon: EditOutlined,
  },
  {
    id: 'allocation',
    title: 'Allocations',
    icon: UsergroupAddOutlined,
  },
  {
    id: 'timetable',
    title: 'Time Table',
    icon: CalendarOutlined,
  },
  {
    id: 'profile',
    title: 'Profile',
    icon: UserOutlined,
  },
]
export const studentNavigation = [
  {
    id: 'home',
    title: 'Home',
    icon: HomeOutlined,
  },
  // {
  //   id: 'timetable',
  //   title: 'Time Table',
  //   icon: CalendarOutlined,
  // },
  {
    id: 'profile',
    title: 'Profile',
    icon: UserOutlined,
  },
]

export const teacherNavigation = [
  {
    id: 'home',
    title: 'Home',
    icon: HomeOutlined,
  },
  {
    id: 'marks',
    title: 'Marks',
    icon: FileWordOutlined,
  },
  {
    id: 'viewTimeTable',
    title: 'View Time Table',
    icon: CalendarOutlined,
  },
  {
    id: 'outline',
    title: 'Course Outline',
    icon: CalendarOutlined,
  },
  {
    id: 'profile',
    title: 'Profile',
    icon: UserOutlined,
  },
]

export const checkAccountAccess = (accountType: string, link: string) => {
  const navList =
    accountType === 'admin'
      ? adminNavigation
      : accountType === 'student'
      ? studentNavigation
      : accountType === 'teacher'
      ? teacherNavigation
      : []

  return navList.filter((nav: any) => nav.id === link).length !== 0
}
