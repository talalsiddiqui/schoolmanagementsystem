import React from 'react'
import { Form, Input, Button, Select } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import styles from './index.module.scss'
import {
  Translate,
  LocalizeContextProps,
  withLocalize,
} from 'react-localize-redux'

const { Option } = Select

interface IProps extends LocalizeContextProps {
  onClickLogin: () => any
  submitedData: any
  signUpProgress: boolean
}

const SignUp = ({
  onClickLogin,
  submitedData,
  signUpProgress,
  translate,
}: IProps) => {
  const onFinish = (fieldsValue: any) => {
    const values = {
      ...fieldsValue,
    }
    submitedData(values)
  }
  const clickSignUp = () => {
    onClickLogin()
  }

  return (
    <Form
      name="signUpForm"
      className={styles.loginForm}
      initialValues={{ remember: true }}
      onFinish={onFinish}
    >
      <Form.Item
        className={styles.formItem}
        name="email"
        rules={[
          {
            required: true,
            message: translate('auth.validations.emailRequired').toString(),
          },
          {
            type: 'email',
            message: translate('auth.validations.validEmail').toString(),
          },
        ]}
      >
        <Input
          size="large"
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder={translate('common.email').toString()}
        />
      </Form.Item>
      <Form.Item
        className={styles.formItem}
        name="password"
        rules={[
          {
            required: true,
            message: translate('auth.validations.passwordRquired').toString(),
          },
          {
            min: 6,
            message: translate('auth.validations.minPasswordLength').toString(),
          },
        ]}
      >
        <Input
          size="large"
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder={translate('common.password').toString()}
        />
      </Form.Item>

      <Form.Item
        className={styles.formItem}
        name="name"
        rules={[
          {
            required: true,
            message: translate('auth.validations.firstNameRequired').toString(),
          },
        ]}
      >
        <Input
          size="large"
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder={translate('common.firstName').toString()}
        />
      </Form.Item>
      <Form.Item
        className={styles.formItem}
        name="accountType"
        rules={[
          {
            required: true,
            message: 'Select Account Type',
          },
        ]}
      >
        <Select allowClear placeholder="Select Account Type">
          <Option value="student">Student</Option>
          <Option value="teacher">Teacher</Option>
        </Select>
      </Form.Item>

      <Form.Item>
        <Button
          type="primary"
          size="large"
          block
          htmlType="submit"
          className="login-form-button"
          shape="round"
          loading={signUpProgress}
        >
          <Translate id="auth.sign.signUp" />
        </Button>
      </Form.Item>
      <div className={styles.links}>
        <h3 className={styles.noAccountText}>
          <Translate id="auth.sign.accountExists" />
        </h3>
        <h3 className={styles.login} onClick={clickSignUp}>
          <Translate id="auth.login.login" />
        </h3>
      </div>
    </Form>
  )
}

export default withLocalize(SignUp)
