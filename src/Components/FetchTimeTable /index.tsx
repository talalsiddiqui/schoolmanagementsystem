/* eslint-disable no-unused-vars */
import { useEffect, useState } from 'react'
import { useFirestoreConnect } from 'react-redux-firebase'
import { useSelector } from 'react-redux'
import { IRootState } from '../../reducers'

const FetchTimeTable = () => {
  const [timeTableDisplay, setTimeTableDisplay] = useState<any>([])

  useFirestoreConnect(() => [{ collection: 'timetable' }])

  const allTimeTable = useSelector(
    (state: IRootState) => state.firestore.data.timetable,
  )

  const getTimeTable = (timeTableData: any) => {
    const data = []

    // eslint-disable-next-line no-unused-vars
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    for (const [key, value] of Object.entries(timeTableData)) {
      data.push(value)
    }
    return data
  }

  useEffect(() => {
    const TimetableData = allTimeTable && Object.keys(allTimeTable)
    const tempData: any = []
    TimetableData &&
      TimetableData.map(className => {
        for (const [key, value] of Object.entries(allTimeTable[className])) {
          tempData.push({
            class: className,
            day: key,
            data: getTimeTable(value),
          })
        }
      })

    setTimeTableDisplay(tempData)
  }, [allTimeTable])

  return timeTableDisplay
}

export default FetchTimeTable
