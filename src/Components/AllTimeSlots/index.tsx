import { useEffect, useState } from 'react'
import { useFirestoreConnect } from 'react-redux-firebase'
import { useSelector } from 'react-redux'
import { IRootState } from '../../reducers'

const AllTimeSlots = () => {
  const [allTimeSlotsArray, setAllTimeSlotsArray] = useState<any>([])

  useFirestoreConnect(() => [{ collection: 'timeSlots' }])

  const allTimeSlotsData = useSelector(
    (state: IRootState) => state.firestore.data.timeSlots,
  )

  const tempTimeSlots: {
    key: string
    time: string
  }[] = []
  useEffect(() => {
    const allTimeSlotsID = allTimeSlotsData && Object.keys(allTimeSlotsData)

    allTimeSlotsID &&
      allTimeSlotsID.map(time => {
        tempTimeSlots.push({
          key: time,
          time: time,
        })
      })

    setAllTimeSlotsArray(tempTimeSlots)
  }, [allTimeSlotsData])

  return allTimeSlotsArray
}

export default AllTimeSlots
