import { useSelector } from 'react-redux'
import { IRootState } from '../../reducers'

const AccountDetails = () => {
  const { profile } = useSelector((state: IRootState) => state.firebase)

  return profile
}

export default AccountDetails
