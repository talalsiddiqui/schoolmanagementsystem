import React, { useState, useEffect } from 'react'
import { Form, Input, Button, Modal, message, Typography } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import {
  Translate,
  LocalizeContextProps,
  withLocalize,
} from 'react-localize-redux'
import styles from './index.module.scss'

const { Text } = Typography

interface IProps extends LocalizeContextProps {
  onClickSign: () => any
  submitedData: any
  loginProgress: boolean
  forgetPassword: (email: string) => void
  forgetPasswordProgress: boolean
  forgetPasswordErrorMessage: undefined | string
  forgetPasswordSuccess: boolean
}

const Login = ({
  onClickSign,
  loginProgress,
  submitedData,
  forgetPasswordProgress,
  forgetPassword,
  forgetPasswordErrorMessage,
  forgetPasswordSuccess,
  translate,
}: IProps) => {
  const [forgetPasswordVisible, setForgetPasswordVisible] = useState(false)
  const [emailID, setEmailID] = useState<undefined | string>(undefined)

  useEffect(() => {
    if (!forgetPasswordProgress && forgetPasswordSuccess) {
      message.success(translate('auth.login.forgotPasswordEmail'))
      setForgetPasswordVisible(false)
    }
  }, [forgetPasswordSuccess, forgetPasswordProgress])

  const onFinish = (values: any) => {
    submitedData(values)
  }
  const clickSignUp = () => {
    onClickSign()
  }

  const onOKPress = (e: any) => {
    console.log(e)
    emailID && forgetPassword(emailID)
  }

  const onCancelPress = () => {
    setForgetPasswordVisible(false)
  }

  const onForgetPasswordEmailChange = (e: any) => {
    const email = e.target.value
    setEmailID(email)
  }
  return (
    <>
      <Form
        name="loginForm"
        className={styles.loginForm}
        initialValues={{ remember: true }}
        onFinish={onFinish}
      >
        <Form.Item
          className={styles.formItem}
          name="email"
          rules={[
            {
              required: true,
              message: translate('auth.validations.emailRequired').toString(),
            },
            {
              type: 'email',
              message: translate('auth.validations.validEmail').toString(),
            },
          ]}
        >
          <Input
            size="large"
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder={translate('common.email').toString()}
          />
        </Form.Item>
        <Form.Item
          className={styles.formItem}
          name="password"
          rules={[
            {
              required: true,
              message: translate('auth.validations.passwordRquired').toString(),
            },
            {
              min: 6,
              message: translate(
                'auth.validations.minPasswordLength',
              ).toString(),
            },
          ]}
        >
          <Input
            size="large"
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder={translate('common.password').toString()}
          />
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            size="large"
            block
            loading={loginProgress}
            htmlType="submit"
            className="login-form-button"
            shape="round"
          >
            <Translate id="auth.login.login" />
          </Button>
        </Form.Item>
        <div className={styles.links}>
          <h3 className={styles.noAccountText}>
            <Translate id="auth.login.noAccount" />
          </h3>
          <h3 className={styles.signUp} onClick={clickSignUp}>
            <Translate id="auth.sign.signUp" />
          </h3>
          <h4
            className={styles.forgetPassword}
            onClick={() => setForgetPasswordVisible(true)}
          >
            <Translate id="auth.login.forgotPassword" />
          </h4>
        </div>
      </Form>
      <Modal
        title="Title"
        visible={forgetPasswordVisible}
        onOk={onOKPress}
        confirmLoading={forgetPasswordProgress}
        onCancel={onCancelPress}
      >
        <p>
          <Input
            value={emailID}
            autoFocus
            placeholder={translate('common.emailPlaceholder').toString()}
            onChange={onForgetPasswordEmailChange}
          />
          {forgetPasswordErrorMessage && (
            <Text type="danger">{forgetPasswordErrorMessage}</Text>
          )}
        </p>
      </Modal>
    </>
  )
}

export default withLocalize(Login)
