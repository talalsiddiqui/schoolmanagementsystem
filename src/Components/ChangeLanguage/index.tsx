import React, { useState } from 'react'
import { withLocalize, LocalizeContextProps } from 'react-localize-redux'
import { setUserLang } from '../../utils/sessions'
import { Select } from 'antd'
const { Option } = Select

interface IProps extends LocalizeContextProps {}

const LanguageToggle = ({
  languages,
  activeLanguage,
  setActiveLanguage,
}: IProps) => {
  const [langValue, setLangValue] = useState(activeLanguage.code)

  const translationChange = (langCode: string) => {
    setLangValue(langCode)
    setActiveLanguage(langCode)
    setUserLang(langCode)
  }

  return (
    <Select defaultValue="lucy" value={langValue} onChange={translationChange}>
      {languages.map((lang, index) => (
        <Option value={lang.code} key={index}>
          {lang.name}
        </Option>
      ))}
    </Select>
  )
}

export default withLocalize(LanguageToggle)
