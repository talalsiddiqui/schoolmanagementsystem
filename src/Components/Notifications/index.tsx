import { notification } from 'antd'
interface IProps {
  message: string
  description?: string
  placement?: 'bottomRight' | 'topLeft' | 'topRight' | 'bottomLeft'
  type: 'success' | 'info' | 'warning' | 'error'
}

const notificationDisplay = ({
  message,
  description,
  placement,
  type,
}: IProps) => {
  return notification[type]({
    message,
    description,
    placement: placement === undefined ? 'topRight' : placement,
  })
}

export default notificationDisplay
