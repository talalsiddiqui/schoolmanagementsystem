import { useEffect, useState } from 'react'
import { useFirestoreConnect } from 'react-redux-firebase'
import { useSelector } from 'react-redux'
import { IRootState } from '../../reducers'

const AllStudents = () => {
  const [allStudentsArray, setAllStudentsArray] = useState<any>([])

  useFirestoreConnect(() => [
    { collection: 'users', where: [['accountType', '==', 'student']] },
  ])

  const allStudentsData = useSelector(
    (state: IRootState) => state.firestore.data.users,
  )
  const tempStudents: {
    key: string
    name: string
    class: string
  }[] = []
  useEffect(() => {
    const allStudentsID = allStudentsData && Object.keys(allStudentsData)

    allStudentsID &&
      allStudentsID.map(teacherID => {
        tempStudents.push({
          key: teacherID,
          name: allStudentsData[teacherID].name,
          class: allStudentsData[teacherID].class,
        })
      })

    setAllStudentsArray(tempStudents)
  }, [allStudentsData])

  return allStudentsArray
}

export default AllStudents
