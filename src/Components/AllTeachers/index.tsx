import { useEffect, useState } from 'react'
import { useFirestoreConnect } from 'react-redux-firebase'
import { useSelector } from 'react-redux'
import { IRootState } from '../../reducers'

const AllTeachers = () => {
  const [allTeachersArray, setAllTeachersArray] = useState<any>([])

  useFirestoreConnect(() => [
    { collection: 'users', where: [['accountType', '==', 'teacher']] },
  ])

  const allTeachersData = useSelector(
    (state: IRootState) => state.firestore.data.users,
  )
  const tempTeachers: {
    key: string
    name: string
    subject: string
  }[] = []
  useEffect(() => {
    const allTeachersID = allTeachersData && Object.keys(allTeachersData)

    allTeachersID &&
      allTeachersID.map(teacherID => {
        tempTeachers.push({
          key: teacherID,
          name: allTeachersData[teacherID].name,
          subject: allTeachersData[teacherID].subject,
        })
      })

    setAllTeachersArray(tempTeachers)
  }, [allTeachersData])

  return allTeachersArray
}

export default AllTeachers
