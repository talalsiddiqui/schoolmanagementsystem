import React, { useEffect } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { LocalizeContextProps, withLocalize } from 'react-localize-redux'
import { renderToStaticMarkup } from 'react-dom/server'
import Auth from '../Containers/Auth'
import PrivateRoute from './PrivateRouting'
import AccountApprove from '../Containers/AccountApprove'
import ScheduleParameters from '../Containers/ScheduleParameters'
import StudentFees from '../Containers/StudentFees'
import Allocation from '../Containers/Allocation'
import TimeTable from '../Containers/TimeTable'
import { getUserlang } from '../utils/sessions'
import globalTranslations from '../translations/global.json'
import Home from '../Containers/Home'
import SubjectMarks from '../Containers/Teacher/Marks'
import ViewTimeTable from '../Containers/Teacher/ViewTimeTable'
import Outline from '../Containers/Teacher/Outline'

interface IRoutingProps extends LocalizeContextProps {}

const Routing = ({ initialize }: IRoutingProps) => {
  useEffect(() => {
    initialize({
      languages: [
        { name: 'English', code: 'en' },
        { name: 'Spanish', code: 'es' },
      ],
      options: {
        renderToStaticMarkup,
        defaultLanguage: getUserlang(),
      },
      translation: globalTranslations,
    })
  }, [])

  return (
    <Switch>
      <Route exact path="/auth" component={Auth} />
      <PrivateRoute exact path="/" component={Home} />
      <PrivateRoute exact path="/home" component={Home} />
      <PrivateRoute exact path="/accounts" component={AccountApprove} />
      <PrivateRoute exact path="/studentFees" component={StudentFees} />
      <PrivateRoute exact path="/parameters" component={ScheduleParameters} />
      <PrivateRoute exact path="/allocation" component={Allocation} />
      <PrivateRoute exact path="/timetable" component={TimeTable} />
      <PrivateRoute exact path="/marks" component={SubjectMarks} />
      <PrivateRoute exact path="/viewTimeTable" component={ViewTimeTable} />
      <PrivateRoute exact path="/outline" component={Outline} />
      <Redirect to="/" />
    </Switch>
  )
}

export default withLocalize(Routing)
