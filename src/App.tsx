import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import Routes from './routing/Routing'
import storeConfig from './configureStore'
import { Provider } from 'react-redux'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import { ReactReduxFirebaseProvider } from 'react-redux-firebase'
import { fbConfig } from './config/firebase'
import { createFirestoreInstance } from 'redux-firestore'
import { LocalizeProvider } from 'react-localize-redux'

import 'antd/dist/antd.less'

const rrfConfig = {
  userProfile: 'users',
  useFirestoreForProfile: true,
}

firebase.initializeApp(fbConfig)
firebase.firestore()

const store = storeConfig()

const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch,
  createFirestoreInstance,
}

function App() {
  return (
    <LocalizeProvider>
      <Provider store={store}>
        <ReactReduxFirebaseProvider {...rrfProps}>
          <Router>
            <Routes />
          </Router>
        </ReactReduxFirebaseProvider>
      </Provider>
    </LocalizeProvider>
  )
}

export default App
