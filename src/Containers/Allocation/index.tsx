import React, { useEffect, useState } from 'react'
import { Card, Table, Select, Row, Col, Button } from 'antd'
import { useSelector } from 'react-redux'
import { IRootState } from '../../reducers'
import styles from './index.module.scss'
import { useFirestore, useFirestoreConnect } from 'react-redux-firebase'
import notificationDisplay from '../../Components/Notifications'

const { Option } = Select

const ScheduleParameters = () => {
  const [selectedTeacher, setSelectedTeacher] = useState<string>()
  const [selectedStudent, setSelectedStudent] = useState<string>()
  const [selectedSubject, setSelectedSubject] = useState<string>()
  const [selectedClass, setSelectedClass] = useState<string>()
  const [allTeachersDisplay, setAllTeachersDisplay] = useState<any>([])
  const [allStudentsDisplay, setAllStudentsDisplay] = useState<any>([])
  const [allSubjectsDisplay, setAllSubjectsDisplay] = useState<any>([])
  const [allClassesDisplay, setAllClassesDisplay] = useState<any>([])

  useFirestoreConnect(() => [
    { collection: 'users', where: [['accountType', '==', 'teacher']] },
    {
      collection: 'users',
      storeAs: 'students',
      where: [['accountType', '==', 'student']],
    },
    { collection: 'subjects' },
    { collection: 'classes' },
  ])
  const firestore = useFirestore()
  const allTeachers = useSelector(
    (state: IRootState) => state.firestore.data.users,
  )
  const allStudents = useSelector(
    (state: IRootState) => state.firestore.data.students,
  )

  const allSubjects = useSelector(
    (state: IRootState) => state.firestore.data.subjects,
  )
  const allClasses = useSelector(
    (state: IRootState) => state.firestore.data.classes,
  )

  useEffect(() => {
    if (allTeachers) {
      const allTeachersKeys = allTeachers && Object.keys(allTeachers)
      const tempTeachers: {
        key: string
        name: string
        subject: string
      }[] = []
      allTeachersKeys.map(teacherDocID => {
        tempTeachers.push({
          key: teacherDocID,
          name: allTeachers[teacherDocID].name,
          subject: allTeachers[teacherDocID].subject,
        })
      })
      setAllTeachersDisplay(tempTeachers)
    }
    if (allStudents) {
      const allStudentsKeys = allStudents && Object.keys(allStudents)
      const tempStudents: {
        key: string
        name: string
        class: string
      }[] = []
      allStudentsKeys.map(studentDocID => {
        tempStudents.push({
          key: studentDocID,
          name: allStudents[studentDocID].name,
          class: allStudents[studentDocID].class,
        })
      })
      setAllStudentsDisplay(tempStudents)
    }
    if (allSubjects) {
      const allSubjectsID = allSubjects && Object.keys(allSubjects)
      const tempSubjects: {
        key: string
        name: string
      }[] = []
      allSubjectsID.map(subjectID => {
        tempSubjects.push({
          key: subjectID,
          name: subjectID,
        })
      })
      setAllSubjectsDisplay(tempSubjects)
    }
    if (allClasses) {
      const allClassesID = allClasses && Object.keys(allClasses)
      const tempClasses: {
        key: string
        name: string
      }[] = []
      allClassesID.map(classID => {
        tempClasses.push({
          key: classID,
          name: classID,
        })
      })
      setAllClassesDisplay(tempClasses)
    }
  }, [allTeachers, allSubjects, allStudents, allClasses])

  const onTeacherChange = (teacherID: string) => {
    setSelectedTeacher(teacherID)
  }
  const onStudentChange = (studentID: string) => {
    setSelectedStudent(studentID)
  }

  const onSubjectChange = (subject: string) => {
    setSelectedSubject(subject)
  }
  const onClassChange = (className: string) => {
    setSelectedClass(className)
  }

  const teacherSubjectColumns = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Subject',
      dataIndex: 'subject',
    },
  ]
  const studentClassColumns = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Class',
      dataIndex: 'class',
    },
  ]

  const checkSubjectAlreadyAllocated = (subject: string) => {
    return (
      allTeachersDisplay.filter(
        (teacher: { subject: string }) => teacher.subject === subject,
      ).length > 0
    )
  }

  const onTeacherSubjectEnter = () => {
    if (selectedSubject === undefined && selectedTeacher === undefined) {
      notificationDisplay({ message: 'Select All Fields', type: 'error' })
    } else if (
      selectedSubject &&
      !checkSubjectAlreadyAllocated(selectedSubject)
    ) {
      firestore
        .collection('users')
        .doc(selectedTeacher)
        .set({ subject: selectedSubject }, { merge: true })
        .then(function() {
          notificationDisplay({
            message: `Teacher assigned subject ${selectedSubject} successfully added`,
            type: 'success',
          })
        })
        .catch((error: any) => {
          notificationDisplay({
            message: `Error ${error}`,
            type: 'error',
          })
        })
    } else {
      notificationDisplay({
        message: 'Subject already assigned to a teacher',
        type: 'error',
      })
    }
  }
  const onStudentClassEnter = () => {
    if (selectedClass && selectedStudent) {
      firestore
        .collection('users')
        .doc(selectedStudent)
        .set({ class: selectedClass }, { merge: true })
        .then(function() {
          notificationDisplay({
            message: `Student assigned subject ${selectedClass} successfully added`,
            type: 'success',
          })
        })
        .catch((error: any) => {
          notificationDisplay({
            message: `Error ${error}`,
            type: 'error',
          })
        })
    } else {
      notificationDisplay({ message: 'Select All Fields', type: 'error' })
    }
  }

  return (
    <div className={styles.layoutContent}>
      <Card size="default" title="Add Subject For Teacher">
        <Row gutter={16}>
          <Col xs={24} md={12}>
            <Select
              className={styles.topMargin1}
              placeholder="Select Teacher"
              style={{ width: '100%' }}
              onChange={onTeacherChange}
              allowClear
            >
              {allTeachersDisplay &&
                allTeachersDisplay.map(
                  (teacher: { key: string | number; name: string }) => (
                    <Option key={teacher.key} value={teacher.key}>
                      {teacher.name}
                    </Option>
                  ),
                )}
            </Select>

            <Select
              placeholder="Select Subject"
              style={{ width: '100%', marginTop: '1rem' }}
              onChange={onSubjectChange}
              allowClear
            >
              {allSubjectsDisplay &&
                allSubjectsDisplay.map(
                  (subject: { key: string; name: string }) => (
                    <Option key={subject.key} value={subject.name}>
                      {subject.name}
                    </Option>
                  ),
                )}
            </Select>

            <Button
              className={styles.topMargin1}
              type="primary"
              onClick={onTeacherSubjectEnter}
            >
              Enter
            </Button>
          </Col>
          <Col xs={24} md={12}>
            <Table
              pagination={false}
              columns={teacherSubjectColumns}
              dataSource={allTeachersDisplay}
            />
          </Col>
        </Row>
      </Card>

      <Card size="default" title="Add Class For Student">
        <Row gutter={16}>
          <Col xs={24} md={12}>
            <Select
              className={styles.topMargin1}
              placeholder="Select Student"
              style={{ width: '100%' }}
              onChange={onStudentChange}
              allowClear
            >
              {allStudentsDisplay &&
                allStudentsDisplay.map(
                  (student: { key: string | number; name: string }) => (
                    <Option key={student.key} value={student.key}>
                      {student.name}
                    </Option>
                  ),
                )}
            </Select>

            <Select
              placeholder="Select Class"
              style={{ width: '100%', marginTop: '1rem' }}
              onChange={onClassChange}
              allowClear
            >
              {allClassesDisplay &&
                allClassesDisplay.map(
                  (classes: { key: string; name: string }) => (
                    <Option key={classes.key} value={classes.name}>
                      {classes.name}
                    </Option>
                  ),
                )}
            </Select>

            <Button
              className={styles.topMargin1}
              type="primary"
              onClick={onStudentClassEnter}
            >
              Enter
            </Button>
          </Col>
          <Col xs={24} md={12}>
            <Table
              pagination={false}
              columns={studentClassColumns}
              dataSource={allStudentsDisplay}
            />
          </Col>
        </Row>
      </Card>
    </div>
  )
}

export default ScheduleParameters
