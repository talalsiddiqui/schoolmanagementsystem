import React from 'react'
import { Layout, Row, Button, Dropdown, Menu, Result } from 'antd'
import {
  adminNavigation,
  studentNavigation,
  teacherNavigation,
  checkAccountAccess,
} from '../../utils/navigations'
import { withRouter, RouteComponentProps } from 'react-router'
import { useMediaQuery } from 'react-responsive'
import { LogoutOutlined, MailOutlined } from '@ant-design/icons'
import styles from './index.module.scss'
import { useFirebase } from 'react-redux-firebase'
import { useSelector } from 'react-redux'
import { IRootState } from '../../reducers'
import Spinner from '../../Components/Spinner'

const { Header, Content } = Layout
interface IProps extends RouteComponentProps {
  children: any
}

const PageLayout = ({ children, location, history }: IProps) => {
  const firebase = useFirebase()
  const { profile } = useSelector((state: IRootState) => state.firebase)
  const { email, approval, accountType } = profile
  console.log('profile', accountType)

  const handleClick = (id: string) => {
    history.push(`/${id}`)
  }

  const logout = () => {
    firebase.logout()
  }

  const accountMenu = (
    <Menu>
      <Menu.Item>
        <MailOutlined /> <span style={{ marginLeft: '5px' }}>{email}</span>
      </Menu.Item>

      <Menu.Item onClick={logout}>
        <LogoutOutlined />
        <span style={{ marginLeft: '5px' }}>Log out</span>
      </Menu.Item>
    </Menu>
  )

  const navigationList = (account: string) => {
    const navList =
      account === 'admin'
        ? adminNavigation
        : account === 'student'
        ? studentNavigation
        : account === 'teacher'
        ? teacherNavigation
        : []

    return navList.map(
      (navigateIcon: { id: string; icon: any; title: React.ReactNode }) => (
        <Dropdown
          overlay={navigateIcon.id === 'profile' ? accountMenu : <div />}
          placement="bottomCenter"
          key={navigateIcon.id}
        >
          <Button
            type="link"
            onClick={() => handleClick(navigateIcon.id)}
            style={{
              color: active === navigateIcon.id ? '' : '#8a96a3',
            }}
            icon={React.createElement(navigateIcon.icon, {
              className: styles.icons,
            })}
          >
            {navigateIcon.title}
          </Button>
        </Dropdown>
      ),
    )
  }

  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 992px)' })
  const activeIcon = location.pathname.split('/')
  const active = activeIcon[1] !== '' ? activeIcon[1] : 'home'

  return (
    <Layout className={styles.layout}>
      {approval ? (
        <>
          {checkAccountAccess(accountType, active) ? (
            <>
              <Header
                className={`${styles.header} ${isTabletOrMobile &&
                  styles.mobileHeader}`}
              >
                <div className={styles.pageWidth}>
                  <Row justify="space-between">
                    {navigationList(accountType)}
                  </Row>
                </div>
              </Header>
              <Content
                className={styles.pageWidth}
                style={{
                  marginTop: isTabletOrMobile ? '0px' : '40px',
                  marginBottom: isTabletOrMobile ? '40px' : '3px',
                }}
              >
                {children}
              </Content>
            </>
          ) : (
            <Result
              style={{
                marginTop: isTabletOrMobile ? '0px' : '40px',
                marginBottom: isTabletOrMobile ? '40px' : '3px',
              }}
              status="403"
              title="403"
              subTitle="Sorry, you are not authorized to access this page."
              extra={
                <Button type="primary" onClick={() => handleClick('home')}>
                  Home
                </Button>
              }
            />
          )}
        </>
      ) : approval === undefined ? (
        <Spinner size="large" />
      ) : (
        <Result
          style={{
            marginTop: isTabletOrMobile ? '0px' : '40px',
            marginBottom: isTabletOrMobile ? '40px' : '3px',
          }}
          status="403"
          title="403"
          subTitle="Sorry, you are not authorized to access this page."
          extra={
            <Button type="primary" onClick={logout}>
              Logout
            </Button>
          }
        />
      )}
    </Layout>
  )
}

export default withRouter(PageLayout)
