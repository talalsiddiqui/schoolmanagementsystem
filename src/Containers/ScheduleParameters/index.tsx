import React, { useEffect, useState } from 'react'
import { Card, Table, Row, Col, Button, Input, TimePicker } from 'antd'
import { useSelector } from 'react-redux'
import { IRootState } from '../../reducers'
import styles from './index.module.scss'
import { useFirestore, useFirestoreConnect } from 'react-redux-firebase'
import notificationDisplay from '../../Components/Notifications'
import moment from 'moment'

const { RangePicker } = TimePicker

const ScheduleParameters = () => {
  const [selectedTimeSlot, setSelectedTimeSlot] = useState<
    [string, string] | []
  >()
  const [addSubject, setAddSubject] = useState<string>()
  const [addClass, setAddClass] = useState<string>()
  const [allClassesDisplay, setAllClassesDisplay] = useState<any>()
  const [allSubjectsDisplay, setAllSubjectsDisplay] = useState<any>()
  const [allTimeDisplay, setAllTimeDisplay] = useState<any>()

  useFirestoreConnect(() => [
    { collection: 'classes' },
    { collection: 'subjects' },
    { collection: 'timeSlots' },
  ])
  const firestore = useFirestore()
  const allClasses = useSelector(
    (state: IRootState) => state.firestore.data.classes,
  )
  const allSubjects = useSelector(
    (state: IRootState) => state.firestore.data.subjects,
  )
  const allTimeSlots = useSelector(
    (state: IRootState) => state.firestore.data.timeSlots,
  )
  useEffect(() => {
    if (allClasses) {
      const allClassesID = allClasses && Object.keys(allClasses)
      const tempClasses: {
        key: string
        class: string
      }[] = []
      allClassesID.map(classID => {
        tempClasses.push({
          key: classID,
          class: classID,
        })
      })
      setAllClassesDisplay(tempClasses)
    }
    if (allSubjects) {
      const allSubjectsID = allSubjects && Object.keys(allSubjects)
      const tempSubjects: {
        key: string
        subject: string
      }[] = []
      allSubjectsID.map(subjectID => {
        tempSubjects.push({
          key: subjectID,
          subject: subjectID,
        })
      })
      setAllSubjectsDisplay(tempSubjects)
    }
    if (allTimeSlots) {
      const allTimeSlotsID = allTimeSlots && Object.keys(allTimeSlots)
      const tempTime: {
        key: string
        slot: string
      }[] = []
      allTimeSlotsID &&
        allTimeSlotsID.map(subjectID => {
          allTimeSlots[subjectID].slot &&
            tempTime.push({
              key: subjectID,
              slot: `${allTimeSlots[subjectID].slot[0]} to ${allTimeSlots[subjectID].slot[1]}`,
            })
        })
      setAllTimeDisplay(tempTime)
    }
  }, [allClasses, allSubjects, allTimeSlots])

  const classColumn = [
    {
      title: 'Class',
      dataIndex: 'class',
    },
  ]
  const subjectColumn = [
    {
      title: 'Subject',
      dataIndex: 'subject',
    },
  ]
  const timeColumn = [
    {
      title: 'Time Slots',
      dataIndex: 'slot',
    },
  ]

  const onAddClassChange = (e: any) => {
    const newClass = e.target.value
    setAddClass(newClass)
  }
  const onAddSubjectChange = (e: any) => {
    const newSubject = e.target.value
    setAddSubject(newSubject)
  }

  const checkTimeSlotExist = (startTime: string, endTime: string) => {
    return allTimeDisplay
      ? allTimeDisplay.filter(
          (time: { slot: string }) =>
            time.slot === `${startTime} to ${endTime}`,
        ).length < 1
      : true
  }

  const onTimeChange = (time: any, timeString: [string, string]) => {
    checkTimeSlotExist(
      moment(time[0]).format('HH:mm'),
      moment(time[1]).format('HH:mm'),
    )
      ? setSelectedTimeSlot([
          moment(time[0]).format('HH:mm'),
          moment(time[1]).format('HH:mm'),
        ])
      : notificationDisplay({
          message: `Already Exits`,
          type: 'error',
        })
  }

  const onAddClassEnter = () => {
    firestore
      .collection('classes')
      .doc(addClass)
      .set({ class: addClass })
      .then(function() {
        notificationDisplay({
          message: `Class: ${addClass} successfully added`,
          type: 'success',
        })
        setAddClass('')
      })
      .catch((error: any) => {
        notificationDisplay({
          message: `Error ${error}`,
          type: 'error',
        })
      })
  }

  const onAddSubjectEnter = () => {
    firestore
      .collection('subjects')
      .doc(addSubject)
      .set({ subject: addSubject })
      .then(function() {
        notificationDisplay({
          message: `Subject: ${addSubject} successfully added`,
          type: 'success',
        })
        setAddSubject('')
      })
      .catch((error: any) => {
        notificationDisplay({
          message: `Error ${error}`,
          type: 'error',
        })
      })
  }

  const onAddTimeEnter = () => {
    selectedTimeSlot &&
    checkTimeSlotExist(
      moment(selectedTimeSlot[0]).format('HH:mm'),
      moment(selectedTimeSlot[1]).format('HH:mm'),
    )
      ? firestore
          .collection('timeSlots')
          .add({ slot: selectedTimeSlot })
          .then(function() {
            selectedTimeSlot &&
              notificationDisplay({
                message: `Time Slot: ${selectedTimeSlot[0]} to ${selectedTimeSlot[1]} successfully added`,
                type: 'success',
              })
          })
          .catch((error: any) => {
            notificationDisplay({
              message: `Error ${error}`,
              type: 'error',
            })
          })
      : notificationDisplay({
          message: `Select Time or it is already exits`,
          type: 'error',
        })
  }
  return (
    <div className={styles.studentFees}>
      <Card size="default" title="Add Class">
        <Row gutter={16}>
          <Col xs={24} md={12}>
            <Input
              placeholder="Add Class"
              value={addClass}
              onChange={onAddClassChange}
            />
            <Button
              className={styles.topMargin1}
              type="primary"
              onClick={onAddClassEnter}
            >
              Enter
            </Button>
          </Col>
          <Col xs={24} md={12}>
            <Table
              pagination={false}
              columns={classColumn}
              dataSource={allClassesDisplay}
            />
          </Col>
        </Row>
      </Card>
      <Card className={styles.topMargin1} size="default" title="Add Subject">
        <Row gutter={16}>
          <Col xs={24} md={12}>
            <Input
              placeholder="Add Subject"
              value={addSubject}
              onChange={onAddSubjectChange}
            />
            <Button
              className={styles.topMargin1}
              type="primary"
              onClick={onAddSubjectEnter}
            >
              Enter
            </Button>
          </Col>
          <Col xs={24} md={12}>
            <Table
              pagination={false}
              columns={subjectColumn}
              dataSource={allSubjectsDisplay}
            />
          </Col>
        </Row>
      </Card>
      <Card className={styles.topMargin1} size="default" title="Add Time slot">
        <Row gutter={16}>
          <Col xs={24} md={12}>
            <RangePicker
              format="HH:mm"
              picker="time"
              onChange={(time, timeString) => onTimeChange(time, timeString)}
            />

            <br />
            <Button
              className={styles.topMargin1}
              type="primary"
              onClick={onAddTimeEnter}
            >
              Enter
            </Button>
          </Col>
          <Col xs={24} md={12}>
            <Table
              pagination={false}
              columns={timeColumn}
              dataSource={allTimeDisplay}
            />
          </Col>
        </Row>
      </Card>
    </div>
  )
}

export default ScheduleParameters
