import React, { useEffect, useState } from 'react'
import { Card, Table, Select, Row, Col } from 'antd'
import { useSelector } from 'react-redux'
import { IRootState } from '../../../reducers'
import styles from './index.module.scss'
import { useFirestoreConnect } from 'react-redux-firebase'
import FetchTimeTable from '../../../Components/FetchTimeTable '
import AccountDetails from '../../../Components/AccountDetails'

const { Option } = Select

const TeacherTimeTable = () => {
  const [allClassesDisplay, setAllClassesDisplay] = useState<any>([])
  const [allTimeDisplay, setAllTimeDisplay] = useState<any>()
  const [classNameTimeTable, setClassNameTimeTable] = useState<string>()
  const FetchTimeTableData = FetchTimeTable()
  const profile = AccountDetails()
  const { subject } = profile

  useFirestoreConnect(() => [
    { collection: 'subjects' },
    { collection: 'classes' },
    { collection: 'timeSlots' },
  ])

  const allSubjects = useSelector(
    (state: IRootState) => state.firestore.data.subjects,
  )
  const allClasses = useSelector(
    (state: IRootState) => state.firestore.data.classes,
  )

  const allTimeSlots = useSelector(
    (state: IRootState) => state.firestore.data.timeSlots,
  )

  useEffect(() => {
    if (allClasses) {
      const allClassesID = allClasses && Object.keys(allClasses)
      const tempClasses: {
        key: string
        name: string
      }[] = []
      allClassesID.map(classID => {
        tempClasses.push({
          key: classID,
          name: classID,
        })
      })
      setAllClassesDisplay(tempClasses)
    }

    if (allTimeSlots) {
      const allTimeSlotsID = allTimeSlots && Object.keys(allTimeSlots)

      const timeArray: string[] = []

      allTimeSlotsID.map(timeID => {
        allTimeSlots[timeID].slot &&
          timeArray.push(allTimeSlots[timeID].slot[0])
        timeArray.push(allTimeSlots[timeID].slot[1])
      })
      const timeSort = timeArray.sort(function(a: string, b: string) {
        if (parseInt(a.split(':')[0]) - parseInt(b.split(':')[0]) === 0) {
          return parseInt(a.split(':')[1]) - parseInt(b.split(':')[1])
        } else {
          return parseInt(a.split(':')[0]) - parseInt(b.split(':')[0])
        }
      })
      const tempTime: {
        key: number
        slot: string
      }[] = []
      const timeCount: any = []
      timeSort.map((time: string, index: number) => {
        timeCount.push(time)
        if (timeCount.length === 2) {
          tempTime.push({
            key: index,
            slot: `${timeCount[0]} to ${timeCount[1]}`,
          })
          timeCount.splice(0, timeCount.length)
        }
      })
      setAllTimeDisplay(tempTime)
    }
  }, [allSubjects, allClasses, allTimeSlots])

  const teacherSubjectColumns = () => {
    const allTimeTableCol = allTimeDisplay && [...allTimeDisplay]
    return (
      allTimeTableCol &&
      allTimeTableCol.splice(0, 0, { key: 'day', slot: 'Day' }) &&
      allTimeTableCol.map((time: { slot: any }) => {
        return { title: time.slot, dataIndex: time.slot }
      })
    )
  }

  const viewTimeTable = (className: any) => {
    const classTimeTable = FetchTimeTableData.filter(
      (timeTable: { class: any }) => {
        return timeTable.class === className
      },
    )
    const finalClassDayTimeTable: any[] = []

    days.map((day: string) => {
      classTimeTable
        .filter((timeTable: { day: any }) => timeTable.day === day)
        .map((timeTable: any, index: number) => {
          let temp = {}
          timeTable.data.map((data: any) => {
            if (data.subject === subject) {
              const timeSubject = {
                [data.time]: (
                  <a>
                    {data.subject}/<sub>{data.teacher}</sub>
                  </a>
                ),
              }
              temp = { ...timeSubject, ...temp }
            }
          })

          finalClassDayTimeTable.push({
            Day: <a style={{ color: 'red' }}>{timeTable.day}</a>,
            ...temp,
            key: index,
          })
        })
    })
    return finalClassDayTimeTable
  }

  const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
  return (
    <div className={styles.layoutContent}>
      <Card
        size="default"
        style={{ width: '100%' }}
        title="View Time Table For Each Class"
      >
        <Row>
          <Col span={24}>
            <Select
              placeholder="Select Class"
              style={{ width: '100%', marginTop: '1rem' }}
              onChange={(className: string) => setClassNameTimeTable(className)}
              allowClear
            >
              {allClassesDisplay &&
                allClassesDisplay.map(
                  (classes: { key: string; name: string }) => (
                    <Option key={classes.key} value={classes.name}>
                      {classes.name}
                    </Option>
                  ),
                )}
              Row
            </Select>
            <Table
              pagination={false}
              columns={teacherSubjectColumns()}
              dataSource={viewTimeTable(classNameTimeTable)}
            />
          </Col>
        </Row>
      </Card>
    </div>
  )
}

export default TeacherTimeTable
