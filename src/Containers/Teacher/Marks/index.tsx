import React, { useEffect, useState, useCallback } from 'react'
import { Card, Table, Select, Row, Col, Input, Button } from 'antd'
import { useSelector } from 'react-redux'
import { IRootState } from '../../../reducers'
import { useFirestore, useFirestoreConnect } from 'react-redux-firebase'
import notificationDisplay from '../../../Components/Notifications'
import FetchTimeTable from '../../../Components/FetchTimeTable '
import AccountDetails from '../../../Components/AccountDetails'
import AllStudents from '../../../Components/AllStudents'
import styles from './index.module.scss'

const { Option } = Select

const SubjectMarks = () => {
  const [selectedStudent, setSelectedStudent] = useState('')
  const [studentsSubject, setStudentsSubject] = useState<any[]>([])
  const [allMarksDisplay, setAllMarksDisplay] = useState<any>([])

  const [marks, setMarks] = useState('')
  const fullTimeTable = FetchTimeTable()
  const allStudents = AllStudents()
  const profile = AccountDetails()
  const { subject } = profile

  useFirestoreConnect(() => [{ collection: 'marks', doc: subject }])
  const firestore = useFirestore()

  const allMarks = useSelector(
    (state: IRootState) =>
      state.firestore.data.marks && state.firestore.data.marks[subject],
  )

  useEffect(() => {
    if (allMarks) {
      const allMarkSubjects = allMarks && Object.keys(allMarks)
      const tempMarks: {
        key: string
        student: string
        marks: any
      }[] = []

      allMarkSubjects &&
        allMarkSubjects.map((subjectName: any) => {
          tempMarks.push({
            key: subjectName,
            student: subjectName,
            marks: allMarks[subjectName],
          })
        })
      setAllMarksDisplay(tempMarks)
    }
    if (fullTimeTable && allStudents) {
      const subjectClass: any[] = []
      setStudentsSubject([])
      fullTimeTable &&
        fullTimeTable.map((classes: any) => {
          classes.data.map((item: any) => {
            if (item.subject === subject) {
              !subjectClass.includes(classes.class) &&
                subjectClass.push(classes.class)
            }
          })
        })

      subjectClass.map((className: any) => {
        allStudents.map((student: any) => {
          student.class === className &&
            setStudentsSubject(previousData => [student, ...previousData])
        })
      })
    }
  }, [allMarks])

  const onMarks = useCallback(({ target: { value } }) => {
    setMarks(value)
  }, [])

  const onEnterPress = () => {
    firestore
      .collection('marks')
      .doc(subject)
      .set({ [selectedStudent]: marks }, { merge: true })
      .then(function() {
        notificationDisplay({
          message: `Subject: ${subject} marks ${marks} for student ${selectedStudent} successfully added`,
          type: 'success',
        })
        setMarks('')
      })
      .catch((error: any) => {
        notificationDisplay({
          message: `Error ${error}`,
          type: 'error',
        })
      })
  }
  const marksCol = [
    {
      title: 'Students',
      dataIndex: 'student',
    },
    {
      title: 'marks',
      dataIndex: 'marks',
    },
  ]
  return (
    <div className={styles.layoutContent}>
      <Card size="default" title={`Add Student Marks for Subject ${subject}`}>
        <Row>
          <Col md={12}>
            <Select
              onChange={(student: string) => setSelectedStudent(student)}
              style={{ width: '100%' }}
              placeholder="Select Student"
              allowClear
            >
              {studentsSubject &&
                studentsSubject.map((student: any) => (
                  <Option key={student.key} value={student.name}>
                    {student.name}
                  </Option>
                ))}
            </Select>
            <br />
            <br />
            <Input placeholder="Enter Marks" value={marks} onChange={onMarks} />
            <br />
            <br />
            <Button type="primary" onClick={onEnterPress}>
              Enter
            </Button>
          </Col>
        </Row>
      </Card>
      <Card size="default" title="Student Marks">
        <Table
          columns={marksCol}
          dataSource={allMarksDisplay}
          pagination={false}
        />
      </Card>
    </div>
  )
}

export default SubjectMarks
