import React, { useEffect, useState, useCallback } from 'react'
import { Card, Table, Row, Col, Input, Button } from 'antd'
import { useSelector } from 'react-redux'
import { IRootState } from '../../../reducers'
import { useFirestore, useFirestoreConnect } from 'react-redux-firebase'
import notificationDisplay from '../../../Components/Notifications'
import AccountDetails from '../../../Components/AccountDetails'

const { TextArea } = Input

const Outline = () => {
  const [courseOutline, setCourseOutline] = useState('')
  const [outlineDisplay, setOutlineDisplay] = useState<any[]>([])
  const profile = AccountDetails()
  const { subject } = profile

  useFirestoreConnect(() => [
    { collection: 'courseOutline', where: [['subject', '==', subject]] },
  ])
  const firestore = useFirestore()

  const outlineData = useSelector(
    (state: IRootState) => state.firestore.data.courseOutline,
  )

  console.log('outlineData', outlineData)
  useEffect(() => {
    if (outlineData) {
      const allOutline = outlineData && Object.keys(outlineData)
      const tempOutline: {
        key: string
        outline: string
      }[] = []

      allOutline &&
        allOutline.map((docID: any) => {
          tempOutline.push({
            key: docID,
            outline: outlineData[docID].outline,
          })
        })
      setOutlineDisplay(tempOutline)
    }
  }, [outlineData])

  const onOutline = useCallback(({ target: { value } }) => {
    setCourseOutline(value)
  }, [])

  const onEnterPress = () => {
    firestore
      .collection('courseOutline')
      .add({ subject: subject, outline: courseOutline })
      .then(() => {
        notificationDisplay({
          message: `Subject: ${subject} outline successfully added`,
          type: 'success',
        })
        setCourseOutline('')
      })
      .catch((error: any) => {
        notificationDisplay({
          message: `Error ${error}`,
          type: 'error',
        })
      })
  }
  const marksCol = [
    {
      title: 'Outline',
      dataIndex: 'outline',
    },
  ]
  console.log('outlineDisplay ', outlineDisplay)
  return (
    <div style={{ marginTop: '2rem' }}>
      <Card size="default" title={`Add Student Marks for Subject ${subject}`}>
        <Row>
          <Col md={12}>
            <TextArea
              rows={4}
              placeholder="Write Course Outline"
              value={courseOutline}
              onChange={onOutline}
            />
            <br />
            <br />
            <Button type="primary" onClick={onEnterPress}>
              Enter
            </Button>
          </Col>
        </Row>
      </Card>
      <Card size="default" title="Student Marks">
        <Table
          columns={marksCol}
          dataSource={outlineDisplay}
          pagination={false}
        />
      </Card>
    </div>
  )
}

export default Outline
