import React from 'react'
import AccountDetails from '../../Components/AccountDetails'
import { Result, Typography } from 'antd'

const { Title } = Typography

const Home = () => {
  const profile = AccountDetails()
  const { accountType, name, email, class: myClass, subject } = profile
  console.log('profile', profile)

  const Details = () => (
    <div>
      <br />
      <Title level={4}> Name: {name} </Title>
      <Title level={4}> Email: {email} </Title>
      <Title level={4}> Account: {accountType} </Title>
      {accountType === 'student' && (
        <Title style={{ color: 'orange' }} level={4}>
          Class: {myClass}
        </Title>
      )}
      {accountType === 'teacher' && (
        <Title style={{ color: 'orange' }} level={4}>
          Subject: {subject}
        </Title>
      )}
    </div>
  )
  return (
    <div>
      {accountType === 'admin' && (
        <Result status="success" title="You are Admin" />
      )}
      {accountType === 'student' && (
        <Result
          status="success"
          title="Congrats. Your account is approved. "
          subTitle={Details()}
        />
      )}
      {accountType === 'teacher' && (
        <Result
          status="success"
          title="Congrats. Your account is approved. "
          subTitle={Details()}
        />
      )}
    </div>
  )
}

export default Home
