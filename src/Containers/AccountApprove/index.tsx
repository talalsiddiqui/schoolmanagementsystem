import React, { useEffect, useState } from 'react'
import { Card, Table } from 'antd'
import { useSelector } from 'react-redux'
import { IRootState } from '../../reducers'
import styles from './index.module.scss'
import { useFirestore, useFirestoreConnect } from 'react-redux-firebase'

const Home = () => {
  const [approveUsers, setApproveUsers] = useState<any>([])
  useFirestoreConnect(() => [{ collection: 'users' }])
  const firestore = useFirestore()

  const users = useSelector((state: IRootState) => state.firestore.data.users)

  useEffect(() => {
    if (users) {
      const usersDocID = users && Object.keys(users)
      const tempUsers: {
        key: string
        name: any
        email: any
        accountType: any
        docID: string
        approval: string
      }[] = []
      usersDocID.map(docID => {
        if (users[docID].accountType !== 'admin')
          tempUsers.push({
            key: docID,
            name: users[docID].name,
            email: users[docID].email,
            accountType: users[docID].accountType,
            docID: docID,
            approval: users[docID].approval,
          })
      })
      setApproveUsers(tempUsers)
    }
  }, [users])

  const AccountApprove = (docID: string) => {
    firestore
      .collection('users')
      .doc(docID)
      .update({ approval: true })
  }

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'Account Type',
      dataIndex: 'accountType',
    },
    {
      title: 'Action',

      render: (data: any) => (
        <a onClick={() => AccountApprove(data.docID)}>
          {`${data.approval ? 'Approved' : 'Approve'}`}
        </a>
      ),
    },
  ]

  return (
    <div className={styles.home}>
      <Card size="default" title="Account Approval">
        <Table pagination={false} columns={columns} dataSource={approveUsers} />
      </Card>
    </div>
  )
}

export default Home
