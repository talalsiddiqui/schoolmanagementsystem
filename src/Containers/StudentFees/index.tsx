import React, { useEffect, useState } from 'react'
import { Card, Table, Select, Row, Col, Button, Input } from 'antd'
import { useSelector } from 'react-redux'
import { IRootState } from '../../reducers'
import styles from './index.module.scss'
import { useFirestore, useFirestoreConnect } from 'react-redux-firebase'
import notificationDisplay from '../../Components/Notifications'

const { Option } = Select

const StudentFees = () => {
  const [students, setStudents] = useState<any>([])
  const [selectedStudent, setSelectedStudent] = useState<string>()
  const [studentFees, setStudentFees] = useState<string>()
  const [month, setMonth] = useState<string>()

  useFirestoreConnect(() => [
    { collection: 'fees' },
    { collection: 'users', where: [['accountType', '==', 'student']] },
  ])
  const firestore = useFirestore()

  const users = useSelector((state: IRootState) => state.firestore.data.users)
  const allFees = useSelector((state: IRootState) => state.firestore.data.fees)

  useEffect(() => {
    if (users) {
      const usersDocID = users && Object.keys(users)
      const tempUsers: {
        key: string
        name: any
      }[] = []
      usersDocID.map(docID => {
        tempUsers.push({
          key: docID,
          name: users[docID].name,
        })
      })
      setStudents(tempUsers)
    }
  }, [users])

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
  ]

  const feesColumns = [
    {
      title: 'Month',
      dataIndex: 'month',
    },
    {
      title: 'Fees',
      dataIndex: 'fee',
    },
  ]

  const onFeeChange = (e: any) => {
    const fees = e.target.value
    setStudentFees(fees)
  }
  const onStudentChange = (student: string) => {
    setSelectedStudent(student)
  }
  const onMonthChange = (month: string) => {
    setMonth(month)
  }

  const onEnterPress = () => {
    if (selectedStudent && month && studentFees) {
      firestore
        .collection('fees')
        .doc(selectedStudent)
        .set({ [`${month}`]: studentFees }, { merge: true })
    } else {
      notificationDisplay({ message: 'Select All Fields', type: 'error' })
    }
  }

  const expandedRowRender = (record: any) => {
    const studentFee = allFees && Object.keys(allFees)
    const tempUsers: {
      key: string
      month: string
      fee: any
    }[] = []

    studentFee &&
      studentFee.map(docID => {
        if (docID === record.name) {
          for (const [key, value] of Object.entries(allFees[docID])) {
            tempUsers.push({
              key: docID,
              month: key,
              fee: value,
            })
          }
        }
      })

    return (
      <Table pagination={false} columns={feesColumns} dataSource={tempUsers} />
    )
  }
  return (
    <div className={styles.studentFees}>
      <Card size="default" title="Add Student Fee">
        <Row>
          <Col md={12}>
            <Select
              onChange={onStudentChange}
              style={{ width: '100%' }}
              placeholder="Select Student"
              allowClear
            >
              {students.map((student: any) => (
                <Option key={student.key} value={student.name}>
                  {student.name}
                </Option>
              ))}
            </Select>
            <br />
            <br />
            <Select
              onChange={onMonthChange}
              style={{ width: '100%' }}
              placeholder="Select Month"
              allowClear
            >
              <Option value="January">January</Option>
              <Option value="Feburary">Feburary</Option>
              <Option value="March">March</Option>
              <Option value="April">April</Option>
              <Option value="May">May</Option>
              <Option value="June">June</Option>
              <Option value="July">July</Option>
            </Select>
            <br />
            <br />
            <Input placeholder="Enter Fees" onChange={onFeeChange} />
            <br />
            <br />
            <Button type="primary" onClick={onEnterPress}>
              Enter
            </Button>
          </Col>
        </Row>
      </Card>
      <Card size="default" title="Student Fees">
        <Table
          expandable={{ expandedRowRender }}
          pagination={false}
          columns={columns}
          dataSource={students}
        />
      </Card>
    </div>
  )
}

export default StudentFees
