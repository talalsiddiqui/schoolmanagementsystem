import React, { useState, useEffect } from 'react'
import { Row, Col, message } from 'antd'
import loginImg from '../../assets/system.png'
import tempLogo from '../../assets/logo.png'
import Login from '../../Components/Login'
import SignUp from '../../Components/SignUp'
import { Actions } from './action'
import { RouteComponentProps, Redirect } from 'react-router-dom'
import { IRootState } from '../../reducers'
import { useDispatch, useSelector } from 'react-redux'
import { useFirebase, isLoaded, isEmpty } from 'react-redux-firebase'
import Spinner from '../../Components/Spinner'
// import ChangeLanguage from '../../Components/ChangeLanguage'
import styles from './index.module.scss'

const Auth = ({ history }: RouteComponentProps) => {
  const [loginDisplay, setLoginDisplay] = useState(true)
  const [SignUpDisplay, setSignUpDisplay] = useState(false)
  const {
    signUpProgress,
    signUpSuccess,
    signUpFailure,
    signUpErrorMessage,
    loginProgress,
    loginErrorMessage,
    loginFailure,
    loginSuccess,
    forgetPasswordProgress,
    forgetPasswordSuccess,
    forgetPasswordErrorMessage,
  } = useSelector((state: IRootState) => state.authState)

  const auth = useSelector((state: IRootState) => state.firebase.auth)
  const dispatch = useDispatch()
  const firebase = useFirebase()
  useEffect(() => {
    if (!signUpProgress && signUpSuccess) {
      message.success('Congrats, You have signed successfully')
    }
    if (!signUpProgress && signUpFailure) {
      message.error(signUpErrorMessage)
    }
    if (!loginProgress && loginSuccess && isLoaded(auth) && !isEmpty(auth)) {
      message.success('Welcome Back ! ')
      history.push('/')
    }
    if (!loginProgress && loginFailure) {
      message.error(loginErrorMessage)
    }
  }, [
    signUpProgress,
    signUpProgress,
    loginErrorMessage,
    loginFailure,
    loginSuccess,
    auth,
  ])

  const onClickSignUp = () => {
    setLoginDisplay(false)
    setSignUpDisplay(true)
  }
  const onClickLogin = () => {
    setLoginDisplay(true)
    setSignUpDisplay(false)
  }
  const signUpData = (data: any) => {
    dispatch(
      Actions.postSignUpProgress({
        signUp: data,
        firebase,
      }),
    )
  }
  const loginData = (data: any) => {
    dispatch(
      Actions.loginProgress({
        login: data,
        firebase,
      }),
    )
  }
  const forgetPasswordEmailID = (email: string) => {
    dispatch(
      Actions.forgetPasswordProgress({
        email,
        firebase,
      }),
    )
  }
  if (auth.isLoaded && auth.uid) {
    return <Redirect to="/" />
  }
  if (auth.isLoaded && !auth.uid) {
    return (
      <div style={{ backgroundColor: '#62A9BF' }}>
        <div className={styles.loginWrapper}>
          <Row>
            <Col xs={0} sm={0} md={0} lg={14}>
              <div className={styles.loginImg}>
                <img src={loginImg} width="100%" />
              </div>
            </Col>
            <Col xs={24} sm={24} md={24} lg={10}>
              <div className={styles.loginFormWrapper}>
                <img src={tempLogo} width="60%" height="150px" />
                {loginDisplay && (
                  <Login
                    onClickSign={onClickSignUp}
                    loginProgress={loginProgress}
                    submitedData={loginData}
                    forgetPassword={forgetPasswordEmailID}
                    forgetPasswordProgress={forgetPasswordProgress}
                    forgetPasswordErrorMessage={forgetPasswordErrorMessage}
                    forgetPasswordSuccess={forgetPasswordSuccess}
                  />
                )}
                {SignUpDisplay && (
                  <SignUp
                    signUpProgress={signUpProgress}
                    onClickLogin={onClickLogin}
                    submitedData={signUpData}
                  />
                )}
              </div>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
  return <Spinner size="large" />
}

export default Auth
