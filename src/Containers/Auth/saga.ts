import { put, takeLatest } from 'redux-saga/effects'
import { ActionTypes, Actions } from './action'

function* signUp(action: any) {
  const { signUp, firebase } = action.payload
  try {
    yield firebase.createUser(
      { email: signUp.email, password: signUp.password },
      {
        email: signUp.email,
        username: signUp.email,
        name: signUp.name,
        approval: false,
        accountType: signUp.accountType,
        joinDate: firebase.firestore.Timestamp.now(),
      },
    )
    yield put(Actions.postSignUpSuccess())
  } catch (error) {
    console.log('err', error)
    yield put(Actions.postSignUpFailure(error && error.message))
  }
}

function* login(action: any) {
  const { login, firebase } = action.payload
  try {
    yield firebase.login(login)
    yield put(Actions.loginSuccess())
  } catch (error) {
    console.log('err', error)
    yield put(Actions.loginFailure(error && error.message))
  }
}

function* forgetPassword(action: any) {
  const { email, firebase } = action.payload
  try {
    yield firebase.resetPassword(email)

    yield put(Actions.forgetPasswordSuccess())
  } catch (error) {
    console.log('err', error)
    yield put(Actions.forgetPasswordFailure(error && error.message))
  }
}

export default function* authSaga() {
  yield takeLatest(ActionTypes.POST_SIGNUP_PROGRESS, signUp)
  yield takeLatest(ActionTypes.LOGIN_PROGRESS, login)
  yield takeLatest(ActionTypes.FORGET_PASSWORD_PROGRESS, forgetPassword)
}
