import React, { useEffect, useState } from 'react'
import { Card, Table, Select, Row, Col, Button } from 'antd'
import { useSelector } from 'react-redux'
import { IRootState } from '../../reducers'
import styles from './index.module.scss'
import { useFirestore, useFirestoreConnect } from 'react-redux-firebase'
import notificationDisplay from '../../Components/Notifications'
import AllTeachers from '../../Components/AllTeachers'
import FetchTimeTable from '../../Components/FetchTimeTable '

const { Option } = Select

const TimeTable = () => {
  const [selectedSubject, setSelectedSubject] = useState<string>()
  const [selectedClass, setSelectedClass] = useState<string>()
  const [selectedDay, setSelectedDay] = useState<string>()
  const [selectedTime, setSelectedTime] = useState<string>()
  const [allSubjectsDisplay, setAllSubjectsDisplay] = useState<any>([])
  const [allClassesDisplay, setAllClassesDisplay] = useState<any>([])
  const [allTimeDisplay, setAllTimeDisplay] = useState<any>()
  const [selectedTeacher, setSelectedTeacher] = useState<string>()
  const [classNameTimeTable, setClassNameTimeTable] = useState<string>()
  const AllTeachersData = AllTeachers()
  const FetchTimeTableData = FetchTimeTable()

  useFirestoreConnect(() => [
    { collection: 'subjects' },
    { collection: 'classes' },
    { collection: 'timeSlots' },
  ])
  const firestore = useFirestore()

  const allSubjects = useSelector(
    (state: IRootState) => state.firestore.data.subjects,
  )
  const allClasses = useSelector(
    (state: IRootState) => state.firestore.data.classes,
  )

  const allTimeSlots = useSelector(
    (state: IRootState) => state.firestore.data.timeSlots,
  )

  useEffect(() => {
    if (allSubjects) {
      const allSubjectsID = allSubjects && Object.keys(allSubjects)
      const tempSubjects: {
        key: string
        name: string
      }[] = []
      allSubjectsID.map(subjectID => {
        tempSubjects.push({
          key: subjectID,
          name: subjectID,
        })
      })
      setAllSubjectsDisplay(tempSubjects)
    }

    if (allClasses) {
      const allClassesID = allClasses && Object.keys(allClasses)
      const tempClasses: {
        key: string
        name: string
      }[] = []
      allClassesID.map(classID => {
        tempClasses.push({
          key: classID,
          name: classID,
        })
      })
      setAllClassesDisplay(tempClasses)
    }

    if (allTimeSlots) {
      const allTimeSlotsID = allTimeSlots && Object.keys(allTimeSlots)

      const timeArray: string[] = []

      allTimeSlotsID.map(timeID => {
        allTimeSlots[timeID].slot &&
          timeArray.push(allTimeSlots[timeID].slot[0])
        timeArray.push(allTimeSlots[timeID].slot[1])
      })
      const timeSort = timeArray.sort(function(a: string, b: string) {
        if (parseInt(a.split(':')[0]) - parseInt(b.split(':')[0]) === 0) {
          return parseInt(a.split(':')[1]) - parseInt(b.split(':')[1])
        } else {
          return parseInt(a.split(':')[0]) - parseInt(b.split(':')[0])
        }
      })
      const tempTime: {
        key: number
        slot: string
      }[] = []
      const timeCount: any = []
      timeSort.map((time: string, index: number) => {
        timeCount.push(time)
        if (timeCount.length === 2) {
          tempTime.push({
            key: index,
            slot: `${timeCount[0]} to ${timeCount[1]}`,
          })
          timeCount.splice(0, timeCount.length)
        }
      })
      setAllTimeDisplay(tempTime)
    }
  }, [allSubjects, allClasses, allTimeSlots])

  const onSubjectChange = (subject: string) => {
    getTeacherSubject(subject)
    setSelectedSubject(subject)
  }

  const onClassChange = (className: string) => {
    setSelectedClass(className)
  }

  const getTeacherSubject = (subject: string) => {
    const subjectName = AllTeachersData.find((teacher: any) => {
      return teacher.subject === subject
    })
    setSelectedTeacher(subjectName ? subjectName.name : 'Unassigned')
  }

  const teacherSubjectColumns = () => {
    const allTimeTableCol = allTimeDisplay && [...allTimeDisplay]
    return (
      allTimeTableCol &&
      allTimeTableCol.splice(0, 0, { key: 'day', slot: 'Day' }) &&
      allTimeTableCol.map((time: { slot: any }) => {
        return { title: time.slot, dataIndex: time.slot }
      })
    )
  }

  const viewTimeTable = (className: any) => {
    const classTimeTable = FetchTimeTableData.filter(
      (timeTable: { class: any }) => {
        return timeTable.class === className
      },
    )
    const finalClassDayTimeTable: any[] = []

    days.map((day: string) => {
      classTimeTable
        .filter((timeTable: { day: any }) => timeTable.day === day)
        .map((timeTable: any, index: number) => {
          let temp = {}
          timeTable.data.map((data: any) => {
            const timeSubject = {
              [data.time]: (
                <a>
                  {data.subject}/<sub>{data.teacher}</sub>
                </a>
              ),
            }
            temp = { ...timeSubject, ...temp }
          })

          finalClassDayTimeTable.push({
            Day: <a style={{ color: 'red' }}>{timeTable.day}</a>,
            ...temp,
            key: index,
          })
        })
    })
    return finalClassDayTimeTable
  }

  const onTimeTableEnter = () => {
    if (
      selectedClass &&
      selectedSubject &&
      selectedTime &&
      selectedTeacher &&
      selectedDay
    ) {
      firestore
        .collection('timetable')
        .doc(selectedClass)
        .set(
          {
            [selectedDay]: {
              [`${selectedTime}`]: {
                time: selectedTime,
                subject: selectedSubject,
                teacher: selectedTeacher,
              },
            },
          },
          { merge: true },
        )
        .then(function() {
          notificationDisplay({
            message: `TimeTable for class ${selectedClass} on ${selectedDay} at ${selectedTime} is ${selectedSubject} successfully added`,
            type: 'success',
          })
        })
        .catch((error: any) => {
          notificationDisplay({
            message: `Error ${error}`,
            type: 'error',
          })
        })
    } else {
      notificationDisplay({ message: 'Select All Fields', type: 'error' })
    }
  }
  const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
  return (
    <div className={styles.layoutContent}>
      <Card size="default" title="Generate Time Table">
        <Row gutter={16}>
          <Col xs={24} md={12}>
            <Select
              placeholder="Select Class"
              style={{ width: '100%', marginTop: '1rem' }}
              onChange={onClassChange}
              allowClear
            >
              {allClassesDisplay &&
                allClassesDisplay.map(
                  (classes: { key: string; name: string }) => (
                    <Option key={classes.key} value={classes.name}>
                      {classes.name}
                    </Option>
                  ),
                )}
            </Select>
            <Select
              placeholder="Select Day"
              style={{ width: '100%', marginTop: '1rem' }}
              onChange={(day: string) => setSelectedDay(day)}
              allowClear
            >
              {days &&
                days.map((day: string, index: number) => (
                  <Option key={index} value={day}>
                    {day}
                  </Option>
                ))}
            </Select>
            <Select
              placeholder="Select Time Slot"
              style={{ width: '100%', marginTop: '1rem' }}
              onChange={(time: string) => setSelectedTime(time)}
              allowClear
            >
              {allTimeDisplay &&
                allTimeDisplay.map(
                  (timeslot: { key: string; slot: string }) => (
                    <Option key={timeslot.key} value={timeslot.slot}>
                      {timeslot.slot}
                    </Option>
                  ),
                )}
            </Select>

            <Select
              placeholder="Select Subject"
              style={{ width: '100%', marginTop: '1rem' }}
              onChange={onSubjectChange}
              allowClear
            >
              {allSubjectsDisplay &&
                allSubjectsDisplay.map(
                  (subject: { key: string; name: string }) => (
                    <Option key={subject.key} value={subject.name}>
                      {subject.name}
                    </Option>
                  ),
                )}
            </Select>

            <Button
              className={styles.topMargin1}
              type="primary"
              onClick={onTimeTableEnter}
            >
              Enter
            </Button>
          </Col>
        </Row>
      </Card>

      <Card size="default" title="View Time Table For Each Class">
        <Row>
          <Col span={24}>
            <Select
              placeholder="Select Class"
              style={{ width: '100%', marginTop: '1rem' }}
              onChange={(className: string) => setClassNameTimeTable(className)}
              allowClear
            >
              {allClassesDisplay &&
                allClassesDisplay.map(
                  (classes: { key: string; name: string }) => (
                    <Option key={classes.key} value={classes.name}>
                      {classes.name}
                    </Option>
                  ),
                )}
              Row
            </Select>
            <Table
              pagination={false}
              columns={teacherSubjectColumns()}
              dataSource={viewTimeTable(classNameTimeTable)}
            />
          </Col>
        </Row>
      </Card>
    </div>
  )
}

export default TimeTable
